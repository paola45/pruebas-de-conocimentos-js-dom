const body = document.querySelector("body");
const div = document.createElement("div");
const h1 = document.createElement("h1");
const h2 = document.createElement("h2");
const h1Content = document.createTextNode("18:43:05");
const h2Content = document.createTextNode("18 de mayo de 2020");

h1.appendChild(h1Content);
div.appendChild(h1);
body.appendChild(div);
div.appendChild(h2);
h2.appendChild(h2Content);

const formatNum = (num) => {
  return num < 10 ? "0" + num : num;
};

setInterval(() => {
  const now = new Date();

  // Obtenemos las horas, los minutos y los segundos.
  const hours = now.getHours();
  const minutes = now.getMinutes();
  const seconds = now.getSeconds();
  h1.textContent = `${formatNum(hours)}:${formatNum(minutes)}:${formatNum(
    seconds
  )}`;

  const formatDate = now.toLocaleDateString("es-ES", {
    day: "numeric",
    month: "long",
    year: "numeric",
  });
  h2.textContent = formatDate;
}, 1000);
